package com.ldps;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class ApplicationClient {
    /**
     * prend le fichier contenant la liste des commandes, et le charge dans une
     * variable du type Commande qui est retournée
     */
    public ArrayList<String> saisisCommande(String fichierCommandes) {
        ArrayList<String> commandes;
        try {
            commandes = (ArrayList<String>) Files.readAllLines(Paths.get(fichierCommandes)); //récupère les commandes du fichier commandes
            return commandes;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
    * initialise : ouvre les différents fichiers de lecture et écriture
    */
    public void initialise(String fichierCommandes, String fichierSortie) {
        File f = new File(fichierSortie);
        if(!f.exists()){ //Vérifie si le fichier est déjà créé
            try {
                f.createNewFile(); //Créé le fichier si non
                System.out.println("Fichier sortie créé : " + f.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * prend une Commande dûment formatée, et la fait exécuter par le serveur. Le résultat de
     * l’exécution est retournée. Si la commande ne retourne pas de résultat, on retourne null.
     * Chaque appel doit ouvrir une connexion, exécuter, et fermer la connexion. Si vous le
     * souhaitez, vous pourriez écrire six fonctions spécialisées, une par type de commande
     * décrit plus haut, qui seront appelées par traiteCommande(Commande uneCommande)
     */
    public Object traiteCommande(Commande uneCommande, String[] args) {
        Socket clientSocket;

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        try {
            clientSocket = new Socket(host, port);
            System.out.println("\t\tConnecté à " + clientSocket.getRemoteSocketAddress());

            ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream()); // Mise en place sortie
            ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream()); // Mise en place entrée

            //envoie de la commande
            out.writeObject(uneCommande);
            out.flush();
            System.out.println("\t\tLa commande a été envoyée au serveur.");

            String reponse = (String) in.readObject();
            Files.writeString(Path.of("src/com/ldps/reponse.txt"), reponse + "\n", StandardOpenOption.APPEND); //enregistrement dans le fichier sortie

            //fermeture entrée/sortie et socket
            in.close();
            out.close();
            clientSocket.close();
            return reponse;

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * cette méthode vous sera fournie plus tard. Elle indiquera la séquence d’étapes à exécuter
     * pour le test. Elle fera des appels successifs à saisisCommande(BufferedReader fichier) et
     * traiteCommande(Commande uneCommande).
     */
    public void scenario(String fichierCommandes, String[] args) {
        System.out.println("Debut des traitements :");
        ArrayList<String> commandes = saisisCommande(fichierCommandes); //Récupère les commandes dans le fichier

        for (int i = 0; i < commandes.size(); i++) {
            System.out.println("\tTraitement de la commande " + commandes.get(i) + " ...");
            Object resultat = traiteCommande((new Commande(commandes.get(i))), args); //envoie chaque commande au serveur
            System.out.println("\t\tResultat: " + resultat);
        }
        try { //marquage fin enregistrement dans le fichier sortie
            Files.writeString(Path.of("src/com/ldps/reponse.txt"), "________________\n\n", StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Fin des traitements");
    }

    /**
     * programme principal. Prend 4 arguments: 1) hostname du serveur, 2) numero de port,
     * 3) nom fichier commandes, et 4) nom fichier sortie. Cette méthode doit créer une
     * instance de la classe ApplicationClient, l’initialiser, puis exécuter le scénario
     */
    public static void main(String[] args){
        //String hostname = args[0];
        //String port = args[1];
        //String fichierCommandes = args[2];
        //String fichierSortie = args[3];

        ApplicationClient client = new ApplicationClient();
        client.initialise(args[2], args[3]); //fichierCommande et fichierSortie
        client.scenario(args[2], args); //fichierCommande, args[]

    }
}
