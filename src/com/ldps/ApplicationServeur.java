package com.ldps;

import java.io.*;
import java.lang.reflect.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ApplicationServeur {
    private HashMap<String, Object> objets = new HashMap<>();
    private ServerSocket serverSocket;
    private String commandeResultat = "";
    Class c = null;
    /**
     * prend le numéro de port, crée un SocketServer sur le port
     */
    public ApplicationServeur (int port) {
        try {
            serverSocket = new ServerSocket(port); //Création lisaison socket
            System.out.println("Attente client port " + port + "...");
            aVosOrdres();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Se met en attente de connexions des clients. Suite aux connexions, elle lit
     * ce qui est envoyé à travers la Socket, recrée l’objet Commande envoyé par
     * le client, et appellera traiterCommande(Commande uneCommande)
     */
    public void aVosOrdres() {
        System.out.println("En attente d'un client sur le port " + serverSocket.getLocalPort() + "...");
        while(true) {
            try {
                Socket server = serverSocket.accept();
                System.out.println("Connecté à " + server.getRemoteSocketAddress());

                ObjectOutputStream out = new ObjectOutputStream(server.getOutputStream()); // Mise en place sortie
                ObjectInputStream in = new ObjectInputStream(server.getInputStream()); // Mise en place entrée
                //reception de la commande
                Commande commandeRecue = (Commande) in.readObject();

                traiteCommande(commandeRecue); //Traitement de la commande

                //envoi d'une réponse au client
                System.out.println("Commande renvoyée : " + commandeResultat);
                if (commandeResultat != null) {
                    out.writeObject(commandeResultat);
                    commandeResultat = "La commande est incorrecte.";
                } else {
                    out.writeObject("La commande est incorrecte.");
                }
                out.flush();

                in.close();
                out.close();
                server.close();

            } catch (IOException | ClassNotFoundException | InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * prend uneCommande dument formattée, et la traite. Dépendant du type de commande,
     * elle appelle la méthode spécialisée
     */
    public void traiteCommande(Commande uneCommande) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        //recupere le type de la commande et appelle la méthode correspondante
        switch (uneCommande.get0()){
            case "compilation":
                traiterCompilation(uneCommande.get1());
                break;
            case "chargement":
                traiterChargement(uneCommande.get1());
                break;
            case "creation":
                try {
                    traiterCreation(Class.forName(uneCommande.get1()), uneCommande.get2());
                } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException e) {
                    e.printStackTrace();
                }
                break;
            case "lecture":
                traiterLecture(objets.get(uneCommande.get1()), uneCommande.get2());
                break;
            case "ecriture":
                traiterEcriture(objets.get(uneCommande.get1()), uneCommande.get2(), uneCommande.get3());
                break;
            case "fonction": //NE FONCTIONNE PAS
                if((uneCommande.get3()) != null){
                    String[] tabParams = uneCommande.get3().split(",");
                    String[] tabTypes = new String[tabParams.length];
                    for(int i = 0; i < tabParams.length; i++){
                        tabTypes[i] = tabParams[i].split(":")[0];
                    }
                    traiterAppel(objets.get(uneCommande.get1()), uneCommande.get2(), tabTypes, tabParams);
                } else {
                    commandeResultat = "Commande \"fonction\" non implémentée sous cette forme."; //Client
                    System.out.println("Commande \"fonction\" non implémentée sous cette forme."); //Serveur
                }
                break;
        }
    }

    /**
     * traiterLecture : traite la lecture d’un attribut. Renvoies le résultat par le socket
     */
    public void traiterLecture(Object pointeurObjet, String attribut) {
        try {
            attribut = attribut.substring(0, 1).toUpperCase() + attribut.substring(1);

            Method getter = pointeurObjet.getClass().getMethod("get" + attribut);
            Object valeurRetour = getter.invoke(pointeurObjet);
            commandeResultat = attribut + " est " + valeurRetour; //Client
            System.out.println(attribut + " est " + valeurRetour); //Serveur
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * traiterEcriture : traite l’écriture d’un attribut. Confirmes au client que l’écriture
     * s’est faite correctement.
     */
    public void traiterEcriture(Object pointeurObjet, String attribut, Object valeur) {
        try {
            attribut = attribut.substring(0, 1).toUpperCase() + attribut.substring(1);
            Method setter = pointeurObjet.getClass().getMethod("set" + attribut, valeur.getClass());

            //appel de la méthode
            setter.invoke(pointeurObjet, valeur);
            System.out.println(attribut + " est dorénavant : " + valeur); //Serveur
            commandeResultat = attribut + " est dorénavant : " + valeur; //Client

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * traiterCreation : traite la création d’un objet. Confirme au client que la création
     * s’est faite correctement.
     */
    public void traiterCreation(Class classeDeLobjet, String identificateur) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        c = classeDeLobjet;
        Constructor cons = c.getConstructor();
        objets.put(identificateur, cons.newInstance());
        System.out.println("L'objet " + identificateur + " a été créé.");
        commandeResultat = "Nouvel objet créé : " + identificateur;
    }

    /**
     * traiterChargement : traite le chargement d’une classe. Confirmes au client que la création
     * s’est faite correctement.
     */
    public void traiterChargement(String nomQualifie) {
        try {
            Class.forName(nomQualifie);
            commandeResultat = "La classe " + nomQualifie + " a été chargée."; //Client
            System.out.println("La classe " + nomQualifie + " a été chargée."); //Serveur
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * traiterCompilation : traite la compilation d’un fichier source java. Confirme au client
     * que la compilation s’est faite correctement. Le fichier source est donné par son chemin
     * relatif par rapport au chemin des fichiers sources.
     */
    public void traiterCompilation(String cheminRelatifFichierSource) {
        String cheminFichier[] = cheminRelatifFichierSource.split(",");

        String commande = "javac"; //Commande à compiler
        for (String str : cheminFichier) {
            commande = commande + " " + str;
        }
        System.out.println("Commande à compiler : " + commande);
        try {
            Process process = Runtime.getRuntime().exec(commande); //Execute la commande
            process.waitFor();
            commandeResultat = "Le fichier a été compilé."; //Client
            System.out.println("Le fichier a été compilé."); //Serveur
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * traiterAppel : traite l’appel d’une méthode, en prenant comme argument l’objet
     * sur lequel on effectue l’appel, le nom de la fonction à appeler, un tableau de nom de
     * types des arguments, et un tableau d’arguments pour la fonction. Le résultat de la
     * fonction est renvoyé par le serveur au client (ou le message que tout s’est bien
     * passé)
     */
     public void traiterAppel(Object pointeurObjet, String nomFonction, String[] types, Object[] valeurs) {
        commandeResultat = "Commande \"fonction\" non implémentée."; //Client
        System.out.println("Commande \"fonction\" non implémentée."); //Serveur
     }

     /**
     * programme principal. Prend 4 arguments: 1) numéro de port, 2) répertoire source, 3)
     * répertoire classes, et 4) nom du fichier de traces (sortie)
     * Cette méthode doit créer une instance de la classe ApplicationServeur, l’initialiser
     * puis appeler aVosOrdres sur cet objet
     */
    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        ApplicationServeur serveur = new ApplicationServeur(port);
    }
}