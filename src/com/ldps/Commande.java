package com.ldps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/** implémente l'interface Serializable
 * Elle est utilisée pour emmagasiner la description d’une commande, selon le format spécifié ci-haut.
 * Ce sont des instances de cette classe qu’on sérialisera et qu’on enverra à travers les sockets ou RMI.
 * Prévoir un champ pour emmagasiner le résultat.
 */
public class Commande implements Serializable {

    private final ArrayList<String> commandes = new ArrayList<>();

    public Commande(String commande){
        Collections.addAll(commandes, commande.split("#"));
    }

    public String get0(){ //récupère le 1er argument de la commande (la fonction appelée)
        return commandes.get(0);
    }

    public String get1(){ //récupère le 2e argument de la commande
        return commandes.get(1);
    }

    public String get2(){ //récupère le 3e argument de la commande
        if (commandes.size() > 2) {
            return commandes.get(2);
        }
        return null;
    }

    public String get3(){ //récupère le 4e argument de la commande
        if (commandes.size() > 3) {
            return commandes.get(3);
        }
        return null;
    }
}
